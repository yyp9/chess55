package chess;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;
import board.ChessBoard;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Queen;
import pieces.Rook;
import pieces.piece;
/**
 * The driver class of the chess app, that allows for the playing of the 
 * two player game and the running of the program.
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.King
 * @see pieces.Pawn
 * @see pieces.Queen
 * @see pieces.piece
 * @see pieces.Rook
 * @see pieces.Bishop
 * @see pieces.Knight
 * @see board.ChessBoard
 * @see Check
 */
public class Chess {
	
	/**
	 * The main method, that calls upon {@link #chessLoop()} method
	 * @param args the default arguments from the console if any at all
	 */
	public static void main(String[] args) {
		chessLoop();
	}
	
	/**
	 * The chessLoop method runs an infinite loop that checks inputs and prints the respective outputs.
	 * Prints the board after every valid move to allow better display.
	 * Checks for legal moves, as well as check and checkmate of the kings to determine if the move is legal.
	 * Checks if the input is a valid input based on the piece that is moving
	 * Runs loop until a player wins, draws, or resigns, at which point the loop will break and the program will end.
	 */
	public static void chessLoop(){
		char[] prevMoveLoc = new char[2];
		prevMoveLoc[0] ='x';
		prevMoveLoc[1] = '-';
		piece[] prevPiece = new piece[1];		
		boolean move = true;
		String[][] board = ChessBoard.createBoard();
		Hashtable<String, piece> hash = ChessBoard.createHash(board);
		List<String> whiteList = Check.createCheckHash(hash, 'w');
		List<String> blackList = Check.createCheckHash(hash, 'b');
		String whiteKing = "e1";
		String blackKing = "e8";
		String oldKing = "";
		
		
		String[][] tentBoard = ChessBoard.createBoard();
		Hashtable<String, piece> tentHash = ChessBoard.createHash(board);
		List<String> tentWhiteList = Check.createCheckHash(hash, 'w');
		List<String> tentBlackList = Check.createCheckHash(hash, 'b');
		
		
		Scanner sc = new Scanner(System.in);
		int print = 1;
		boolean firstPrint = true;
		boolean isDraw = false;
		boolean isCheck = false;
		//sc.close();
		while(true) {
			tentBoard = cloneBoard(board);
			tentHash = cloneHash(hash);
			tentWhiteList = cloneList(whiteList);
			tentBlackList = cloneList(blackList);
			if(print == 1) {
				//System.out.println(whiteList);
				if(firstPrint) {
					ChessBoard.printCurrentBoard(board);
					System.out.println();
					firstPrint = false;
				}else {
					System.out.println();
					ChessBoard.printCurrentBoard(board);
					System.out.println();
					if(isCheck == true) {
						System.out.println("Check");
					}
				}
			}
			isCheck = false;
			print = 1;
//			System.out.println();
			String input = "";
			if(move) {
				if(isDraw == true) {
					print = 0;
					input = sc.nextLine();
					if(!input.equals("draw")) {
						System.out.println("Illegal move, try again");
						continue;
					}
					sc.close();
					break;
				}
				System.out.print("White move: ");
				input = sc.nextLine();
				move = false;
			}
			else {
				if(isDraw == true) {
					print = 0;
					input = sc.nextLine();
					if(!input.equals("draw")) {
						System.out.println("Illegal move, try again");
						System.out.print("Black move");
						continue;
					}
					sc.close();
					break;
				}
				System.out.print("Black move: ");
				input = sc.nextLine();
				move = true;
			}
			if(input.length() == 11 && (input.substring(6).equals("draw?"))) {
				print = 0;
				isDraw = true;
			}
			else if(input.equals("resign")) {
				if(move == false) {
					System.out.println("Black wins");
					sc.close();
					break;
				}else if(move == true) {
					System.out.println("White wins");
					sc.close();
					break;
				}
			}
			else if(input.length() == 5) {
				String current = input.substring(0,2);
				String next = input.substring(3,5);
				if(!checkMove(move, hash, current, next) || !legalMove(hash, current, next, board)) {
					
					System.out.println("Illegal move, try again");
					move = !move;
					print = 0;
				}
				else {
					
					if(hash.get(current) instanceof King) {
						if(hash.get(current).color == 'w') {
							oldKing = whiteKing;
							whiteKing = next;
						}
						else {
							oldKing = blackKing;
							blackKing = next;
						}
					}
					
					if(!move) {
						if(executeMove(board, hash, current, next, blackList, whiteList) == false) {
							System.out.println("Illegal move, try again");
							move = !move;
							print = 0;
						}
						else {
							enPassantLegality(hash, current, next, prevMoveLoc, prevPiece);
							if(blackList.contains(next)) {
								blackList.remove(next);
							}
							Check.update(whiteList, current, next);
							String check = Check.isCheck(hash, whiteList, blackKing, board);
							
							if(check != null) {

								if(Check.isCheckmate(hash, blackList, whiteList, blackKing, check, board)) {
									System.out.println("Checkmate");
									System.out.println("White wins");
									sc.close();
									break;
								}
								isCheck = true;
							}
							check = Check.isCheck(hash, blackList, whiteKing, board);
							if(check != null) {
								whiteKing = oldKing;
								board = tentBoard;
								hash = tentHash;
								whiteList = tentWhiteList;
								blackList = tentBlackList;
								System.out.println("Illegal move, try again");
								move = !move;
								print = 0;
							}
						}
					}
					else {
						if(executeMove(board, hash, current, next, blackList, whiteList) == false) {
							System.out.println("Illegal move, try again");
							move = !move;
							print = 0;
						}else {
							enPassantLegality(hash, current, next, prevMoveLoc, prevPiece);
							if(whiteList.contains(next)) {
								whiteList.remove(next);
							}
							Check.update(blackList, current, next);
							String check = Check.isCheck(hash, blackList, whiteKing, board);
							
							
							if(check != null) {
								if(Check.isCheckmate(hash, whiteList, blackList, whiteKing, check, board)) {
									System.out.println("Checkmate");
									System.out.println("Black wins");
									sc.close();
									break;
								}
								isCheck = true;
							}
							
							check = Check.isCheck(hash, whiteList, blackKing, board);
							if(check != null) {
								blackKing = oldKing;
								board = tentBoard;
								hash = tentHash;
								whiteList = tentWhiteList;
								blackList = tentBlackList;
								System.out.println("Illegal move, try again");
								move = !move;
								print = 0;
							}
						}
					}
				}
			}
			else if(input.length() == 7) {
				String current = input.substring(0,2);
				String next = input.substring(3,5);
				char promotion = input.charAt(6);
				if(!checkMove(move, hash, current, next) || !legalMove(hash, current, next, board)) {
					System.out.println("Illegal move, try again");
					move = !move;
					print = 0;
				}else {
					piece currPiece = hash.get(current);
					if(!move) {
						if(currPiece instanceof Pawn && executePawnPromotion(hash, current, next, board, promotion)) {
							enPassantLegality(hash, current, next, prevMoveLoc, prevPiece);
							if(blackList.contains(next)) {
								blackList.remove(next);
							}
							Check.update(whiteList, current, next);
							String check = Check.isCheck(hash, whiteList, blackKing, board);
							if(check != null) {

								if(Check.isCheckmate(hash, blackList, whiteList, blackKing, check, board)) {
									System.out.println("Checkmate");
									System.out.println("White wins");
									sc.close();
									break;
								}
								isCheck = true;
							}
							check = Check.isCheck(hash, blackList, whiteKing, board);
							if(check != null) {
								whiteKing = oldKing;
								board = tentBoard;
								hash = tentHash;
								whiteList = tentWhiteList;
								blackList = tentBlackList;
								System.out.println("Illegal move, try again");
								move = !move;
								print = 0;
							}
						}else {
							System.out.println("Illegal move, try again");
							move = !move;
							print = 0;
						}
					}else {
						if(currPiece instanceof Pawn && executePawnPromotion(hash, current, next, board, promotion)) {
							enPassantLegality(hash, current, next, prevMoveLoc, prevPiece);
							if(whiteList.contains(next)) {
								whiteList.remove(next);
							}
							Check.update(blackList, current, next);
							String check = Check.isCheck(hash, blackList, whiteKing, board);
							if(check != null) {
								if(Check.isCheckmate(hash, whiteList, blackList, whiteKing, check, board)) {
									System.out.println("Checkmate");
									System.out.println("Black wins");
									sc.close();
									break;
								}
								isCheck = true;

							}
							check = Check.isCheck(hash, whiteList, blackKing, board);
							if(check != null) {
								blackKing = oldKing;
								board = tentBoard;
								hash = tentHash;
								whiteList = tentWhiteList;
								blackList = tentBlackList;
								System.out.println("Illegal move, try again");
								move = !move;
								print = 0;
							}
						}else {
							System.out.println("Illegal move, try again");
							move = !move;
							print = 0;
						}
					}
				}
			}
			else {
				System.out.println("Illegal move, try again");
				move = !move;
				print = 0;
			}
			
		}
	}
	
	/**
	 * Checks if the move from start to end is a valid move for the calling piece and calls upon
	 * the specific isLegal method for that piece.
	 * @param hash the hashtable storing the type and position of each piece
	 * @param current the start position of the piece being moved in FileRank format
	 * @param next the end position of the piece being moved in FileRank format
	 * @param board the board representing the entire chess board to be displayed in console
	 * @return true if the move from start to end is a valid move for the piece, false otherwise
	 */
	public static boolean legalMove(Hashtable<String, piece> hash, String current, String next, String[][] board) {
		piece newPiece = hash.get(current);
		if(next.charAt(0) > 104 || next.charAt(0) < 97 || Character.getNumericValue(next.charAt(1)) > 8 || Character.getNumericValue(next.charAt(1)) < 1){
			return false;
		}
		if(newPiece.color == 'o' || newPiece == null) {
			return false;
		}
		return newPiece.isLegal(hash, current, next, board);
	}
	/**
	 * This move checks is the piece referred is a pawn that can be promoted to the piece specified by the user and
	 * then promotes that pawn to the piece specified
	 * @param hash the hashtable storing the type and position of each piece
	 * @param current the start position of the piece being moved in FileRank format
	 * @param next the end position of the piece being moved in FileRank format
	 * @param board the board representing the entire chess board to be displayed in console
	 * @param prom refers to the type of pawn promotion, N for Knight, B for bishop, R for rook, Q for Queen
	 * @return true if the Pawn was successfully promoted to the specified piece prom
	 */
	public static boolean executePawnPromotion(Hashtable<String, piece> hash, String current, String next, String[][] board, char prom) {
		int currentCol = (int)current.charAt(0) - 97;
		int currentRow = ((current.charAt(1) - '0') - 8) * -1;
		int nextCol = (int)next.charAt(0) - 97;
		int nextRow = ((next.charAt(1) - '0') - 8) * -1;
		piece currPiece = hash.get(current);
		char c = currPiece.color;
		int currRank = Character.getNumericValue(current.charAt(1));
		int nextRank = Character.getNumericValue(next.charAt(1));
		if(prom == 'R') {
			if(currRank == 7 && nextRank == 8 && c == 'w') {
				board[currentRow][currentCol] = "wR";
				hash.put(next, new Rook(c));
			}else if(currRank == 2 && nextRank == 1 && c == 'b') {
				board[currentRow][currentCol] = "bR";
				hash.put(next, new Rook(c));
			}
		}else if(prom == 'B') {
			if(currRank == 7 && nextRank == 8 && c == 'w') {
				board[currentRow][currentCol] = "wB";
				hash.put(next, new Bishop(c));
			}else if(currRank == 2 && nextRank == 1 && c == 'b') {
				board[currentRow][currentCol] = "bB";
				hash.put(next, new Bishop(c));
			}
		}else if(prom == 'N') {
			if(currRank == 7 && nextRank == 8 && c == 'w') {
				board[currentRow][currentCol] = "wN";
				hash.put(next, new Knight(c));
			}else if(currRank == 2 && nextRank == 1 && c == 'b') {
				board[currentRow][currentCol] = "bN";
				hash.put(next, new Knight(c));
			}
		}else if(prom == 'Q') {
			if(currRank == 7 && nextRank == 8 && c == 'w') {
				board[currentRow][currentCol] = "wQ";
				hash.put(next, new Queen(c));
			}else if(currRank == 2 && nextRank == 1 && c == 'b') {
				board[currentRow][currentCol] = "bQ";
				hash.put(next, new Queen(c));
			}
		}else {
			return false;
		}
		hash.put(current, new piece());
		board[nextRow][nextCol] = board[currentRow][currentCol];
		
		if(currentCol % 2 == 1 && currentRow % 2 == 0) {
			board[currentRow][currentCol] = "##";
		}
		else if(currentCol % 2 == 0 && currentRow % 2 == 1) {
			board[currentRow][currentCol] = "##";
		}
		else {
		board[currentRow][currentCol] = "  ";
		}
		return true;
	}
	/**
	 * This method executes the move from start to end of the specified piece and updates the hash table as well
	 * as the board to be displayed to the console to show the piece at its end position if the move was done
	 * successfully.  Executes Queen promotion, en Passant, Castling moves as well as check if the moves defined are
	 * valid.
	 * @param board the board representing the entire chess board to be displayed in console
	 * @param hash the hashtable storing the type and position of each piece
	 * @param current the start position of the piece being moved in FileRank format
	 * @param next the end position of the piece being moved in FileRank format
	 * @param bList the list of location of all the black pieces on the board
	 * @param wList the list of the location of all the white pieces on the board
	 * @return true if the move was able to be successfully executed, false otherwise
	 */
	public static boolean executeMove(String[][] board, Hashtable<String, piece> hash, String current, String next, List<String> bList, List<String> wList) {
		int currentCol = (int)current.charAt(0) - 97;
		int currentRow = ((current.charAt(1) - '0') - 8) * -1;
		int nextCol = (int)next.charAt(0) - 97;
		int nextRow = ((next.charAt(1) - '0') - 8) * -1;
		piece currPiece = hash.get(current);
		char c = currPiece.color;
		if(currPiece instanceof Pawn) {
			int currRank = Character.getNumericValue(current.charAt(1));
			int nextRank = Character.getNumericValue(next.charAt(1));
			if(currRank == 7 && nextRank == 8 && c == 'w') {
				board[currentRow][currentCol] = "wQ";
				hash.put(next, new Queen(c));
			}else if(currRank == 2 && nextRank == 1 && c == 'b') {
				board[currentRow][currentCol] = "bQ";
				hash.put(next, new Queen(c));
			}else {
				char currentLetter = current.charAt(0);
				int currentNumber = Character.getNumericValue(current.charAt(1));
				char nextLetter = next.charAt(0);
				int nextNumber = Character.getNumericValue(next.charAt(1));
				if(((int)currentLetter + 1 == (int)nextLetter || (int)currentLetter - 1 == (int)nextLetter) && currentNumber + 1 == nextNumber) {
					if(hash.get(next).color == 'o') {
						String currNum = String.valueOf(Character.getNumericValue(current.charAt(1)));
						String oppPawnLoc = next.charAt(0) + currNum + "";
						piece oppPiece = hash.get(oppPawnLoc);
						if(oppPiece.color == 'b') {
							if(bList.contains(oppPawnLoc)) {
								bList.remove(oppPawnLoc);
							}
							if(nextCol % 2 == 1 && currentRow % 2 == 0) {
								board[currentRow][nextCol] = "##";
							}
							else if(nextCol % 2 == 0 && currentRow % 2 == 1) {
								board[currentRow][nextCol] = "##";
							}
							else {
							board[currentRow][nextCol] = "  ";
							}
						}else {
							if (wList.contains(oppPawnLoc)) {
								wList.remove(oppPawnLoc);
							}
							if(nextCol % 2 == 1 && currentRow % 2 == 0) {
								board[currentRow][nextCol] = "##";
							}
							else if(nextCol % 2 == 0 && currentRow % 2 == 1) {
								board[currentRow][nextCol] = "##";
							}
							else {
								board[currentRow][nextCol] = "  ";
							}
						}
						hash.put(oppPawnLoc, new piece());
					}
				}else if(((int)currentLetter + 1 == (int)nextLetter || (int)currentLetter - 1 == (int)nextLetter) && currentNumber - 1 == nextNumber) {
					if(hash.get(next).color == 'o') {
						String currNum = String.valueOf(Character.getNumericValue(current.charAt(1)));
						String oppPawnLoc = next.charAt(0) + currNum + "";
						piece oppPiece = hash.get(oppPawnLoc);
						if(oppPiece.color == 'b') {
							if(bList.contains(oppPawnLoc)) {
								bList.remove(oppPawnLoc);
							}
							if(nextCol % 2 == 1 && currentRow % 2 == 0) {
								board[currentRow][nextCol] = "##";
							}
							else if(nextCol % 2 == 0 && currentRow % 2 == 1) {
								board[currentRow][nextCol] = "##";
							}
							else {
							board[currentRow][nextCol] = "  ";
							}
						}else {
							if (wList.contains(oppPawnLoc)) {
								wList.remove(oppPawnLoc);
							}
							if(nextCol % 2 == 1 && currentRow % 2 == 0) {
								board[currentRow][nextCol] = "##";
							}
							else if(nextCol % 2 == 0 && currentRow % 2 == 1) {
								board[currentRow][nextCol] = "##";
							}
							else {
							board[currentRow][nextCol] = "  ";
							}
						}
						hash.put(oppPawnLoc, new piece());
					}
				}
				
				hash.put(next, hash.get(current));
			}
		}else if(currPiece instanceof King) {
			char currFile = current.charAt(0);
			int currRank = Character.getNumericValue(current.charAt(1));
			char nextFile = next.charAt(0);
			int nextRank = Character.getNumericValue(next.charAt(1));
			if(currFile == 'e' && currRank == 1 && nextFile == 'g' && nextRank == 1 && c == 'w') {
				if(Check.isCheck(hash, bList, "e1", board) != null || Check.isCheck(hash, bList, "g1", board) != null || Check.isCheck(hash, bList, "f1", board) != null) {
					return false;
				}
				hash.put(next, hash.get(current));
				hash.get("h1").hasMoved = true;
				hash.put("f1", hash.get("h1"));
				Check.update(wList, "h1", "f1");
				hash.put("h1", new piece());
				board[7][5] = board[7][7];
				board[7][7] = " ";
			}else if(currFile == 'e' && currRank == 1 && nextFile == 'c' && nextRank == 1 && c == 'w') {
				if(Check.isCheck(hash, bList, "e1", board) != null || Check.isCheck(hash, bList, "d1", board) != null || Check.isCheck(hash, bList, "c1", board) != null) {
					return false;
				}
				hash.put(next, hash.get(current));
				hash.get("a1").hasMoved = true;
				hash.put("d1", hash.get("a1"));
				Check.update(wList, "a1", "d1");
				hash.put("a1", new piece());
				board[7][3] = board[7][0];
				board[7][0] = "##";
			}else if(currFile == 'e' && currRank == 8 && nextFile == 'g' && nextRank == 8 && c == 'b') {
				if(Check.isCheck(hash, wList, "e8", board) != null || Check.isCheck(hash, wList, "g8", board) != null || Check.isCheck(hash, wList, "f8", board) != null) {
					return false;
				}
				hash.put(next, hash.get(current));
				hash.get("h8").hasMoved = true;
				hash.put("f8", hash.get("h8"));
				Check.update(bList, "h8", "f8");
				hash.put("h8", new piece());
				board[0][5] = board[0][7];
				board[0][7] = "##";
			}else if(currFile == 'e' && currRank == 8 && nextFile == 'c' && nextRank == 8 && c == 'b') {
				if(Check.isCheck(hash, wList, "e8", board) != null || Check.isCheck(hash, wList, "c8", board) != null || Check.isCheck(hash, wList, "d8", board) != null) {
					return false;
				}
				hash.put(next, hash.get(current));
				hash.get("a8").hasMoved = true;
				hash.put("d8", hash.get("a8"));
				Check.update(bList, "a8", "d8");
				hash.put("a8", new piece());
				board[0][3] = board[0][0];
				board[0][0] = " ";
			}else {
				hash.put(next, hash.get(current));
			}
		}
		else {
			hash.put(next, hash.get(current));
		}
		hash.put(current, new piece());
		
		board[nextRow][nextCol] = board[currentRow][currentCol];
		
		if(currentCol % 2 == 1 && currentRow % 2 == 0) {
			board[currentRow][currentCol] = "##";
		}
		else if(currentCol % 2 == 0 && currentRow % 2 == 1) {
			board[currentRow][currentCol] = "##";
		}
		else {
		board[currentRow][currentCol] = "  ";
		}
		return true;
	}
	
	/**
	 * This method runs basic input checks, as well as checks for piece moved with the player moving
	 * @param move indicates what player's turn it is.  True for white, false for black
	 * @param hash the hashtable storing the type and position of each piece
	 * @param current the start position of the piece being moved in FileRank format
	 * @param next the end position of the piece being moved in FileRank format
	 * @return true if the start and end moves are input in as a valid format, false otherwise
	 */
	public static boolean checkMove(boolean move, Hashtable<String, piece> hash, String current, String next) {
		piece currentPiece = hash.get(current);
		
		if(next.charAt(0) > 104 || next.charAt(0) < 97 || Character.getNumericValue(next.charAt(1)) > 8 || Character.getNumericValue(next.charAt(1)) < 1){
			return false;
		}
		
		if(currentPiece == null) {
			return false;
		}
		if(!move && currentPiece.color != 'w') {
			return false;
		}
		if(move && currentPiece.color != 'b') {
			return false;
		}
		
		return true;
	}
	
	/**
	 * This method returns a clone duplicate of the hashtable that was taken as an input
	 * @param hash the hashtable to be cloned
	 * @return copy of the hashtable that was taken as the input
	 */
	public static Hashtable<String, piece> cloneHash(Hashtable<String, piece> hash){
		Hashtable<String, piece> clone = new Hashtable<String, piece>();
		for(String key : hash.keySet()) {
			clone.put(key, hash.get(key));
		}
		return clone;
	}
	/**
	 * This method returns a clone duplicate of the board that was taken as an input
	 * @param board the board that is to be cloned
	 * @return copy of the board that was taken as the input
	 */
	public static String[][] cloneBoard(String[][] board){
		String[][] clone = new String[9][9];
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				clone[row][col] = board[row][col];
			}
			
		}
		return clone;
	}
	/**
	 * This method returns a clone duplicate of the list that was taken as an input
	 * @param list the list that is to be cloned
	 * @return copy of the list that was taken as the input
	 */
	public static List<String> cloneList(List<String> list){
		List<String> clone = new ArrayList<String>();
		for(int i = 0; i < list.size(); i++) {
			clone.add(list.get(i));
		}
		return clone;
	}
	
	/**
	 * This method stores the location and the type of the piece that was last moved.  This method also checks for one of the
	 * en Passant conditions.  If previous piece at the specified location was a pawn who had an en passant status of true, but
	 * the opposing player refused to capture the pawn via en passant, then sets the en Passant Status of that pawn to false so
	 * en Passant cannot ever be done again for that pawn.
	 * 
	 * @param hash the hashtable storing the type and position of each piece
	 * @param current the start position of the piece being moved in FileRank format
	 * @param next the end position of the piece being moved in FileRank format
	 * @param prevMoveLoc an array that stores the file and rank of the previous piece that was moved
	 * @param prevPiece stores an instance of the previous piece that was moved
	 */
	public static void enPassantLegality(Hashtable<String, piece> hash, String current, String next, char[] prevMoveLoc, piece[] prevPiece) {
		if(prevMoveLoc[0] == 'x' && prevMoveLoc[1] == '-') {
			prevMoveLoc[0] = next.charAt(0);
			prevMoveLoc[1] = next.charAt(1);
			prevPiece[0] = hash.get(next);
		}else {
			String prevFileRank = String.valueOf(prevMoveLoc[0]) + String.valueOf(prevMoveLoc[1]) + "";
			if(prevPiece[0] instanceof Pawn && hash.get(prevFileRank) instanceof Pawn) {
				hash.get(prevFileRank).enPassantStatus = false;
				prevMoveLoc[0] = next.charAt(0);
				prevMoveLoc[1] = next.charAt(1);
				prevPiece[0] = hash.get(next);
			}
		}
	}
}













