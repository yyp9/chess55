package chess;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import pieces.Pawn;
import pieces.piece;

/** 
 * This class is used for the check and checkmate feature of the app
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.Pawn
 * @see pieces.piece
 */
public class Check {
	/**
	 * Creates a list of locations of pieces depending on the hashtable
	 * @param hash the hashtable storing the type and position of each piece
	 * @param c the color of the piece
	 * @return list of of locations of pieces
	 */
	public static List<String> createCheckHash(Hashtable<String, piece> hash, char c){
		List<String> list = new ArrayList<String>();
		for(String key : hash.keySet()) {
			if(hash.get(key).color == c) {
				list.add(key);
			}
		}
		return list;
	}
	/**
	 * Updates the location of a piece from its current location to its next location
	 * @param list the list of locations of pieces
	 * @param current the current location of the piece that called on this method
	 * @param next the ending location of the piece that called on this method
	 */
	public static void update(List<String> list, String current, String next) {
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).equals(current)) {
				list.remove(i);
				i--;
			}
		}
		list.add(next);
	}
	/**
	 * Checks to see if the king is in check from another piece on the board
	 * and checks for the location of the piece threatening the king
	 * @param hash the hashtable storing the type and position of each piece
	 * @param list the list of locations of pieces
	 * @param king the current location of the king
	 * @param board the board to be printed to the console
	 * @return string of the location of the piece that is threatening the king
	 */
	public static String isCheck(Hashtable<String, piece> hash, List<String> list, String king, String[][] board){
		for(int i = 0; i < list.size(); i++) {
			piece newPiece = hash.get(list.get(i));

			if(newPiece.isLegal(hash, list.get(i), king, board)) {
				//System.out.println(list.get(i));
				return list.get(i);
			}
		}
		return null;
	}
	/**
	 * Checks to see if the king is in check from another piece on the board
	 * @param hash the hashtable storing the type and position of each piece
	 * @param list the list of locations of pieces
	 * @param king king the current location of the king
	 * @param c color of the king
	 * @param board the board to be printed to the console
	 * @return true if a threatening piece to the king exists, false otherwise
	 */
	public static boolean isCheckBool(Hashtable<String, piece> hash, List<String> list, String king, char c, String[][] board){
		
		piece King = hash.get(king);
		hash.remove(king);
		list.remove(king);
		hash.put(king, new Pawn(c));
		for(int i = 0; i < list.size(); i++) {
			piece newPiece = hash.get(list.get(i));

			if(newPiece.isLegal(hash, list.get(i), king, board)) {
				hash.remove(king);
				hash.put(king, King);
				list.add(king);
				//System.out.println(list.get(i));
				return true;
			}
		}
		hash.remove(king);
		hash.put(king, King);
		list.add(king);
		return false;
	}
	/**
	 * Checks to see if the king is in a checkmate
	 * @param hash the hashtable storing the type and position of each piece
	 * @param list the list of locations of pieces 
	 * @param oppList the list of locations of pieces on the opposite side
	 * @param king the location of the current king
	 * @param check the location of the piece that's threatening the king
	 * @param board the board to be printed to the console
	 * @return true if the king is in checkmate, false otherwise
	 */
	public static boolean isCheckmate(Hashtable<String, piece> hash, List<String> list, List<String> oppList, String king, String check, String[][] board) {
		char currentLetter = king.charAt(0);
		int currentNumber = Character.getNumericValue(king.charAt(1));
		piece threatening = hash.get(check);
		String answer = null;
		String up = "" + Character.toString((char)currentLetter) + (char)(currentNumber + 1 + '0');
		String upRight = "" + Character.toString((char)(currentLetter + 1)) + (char)(currentNumber + 1 + '0');
		String right = "" + Character.toString((char)(currentLetter + 1)) + (char)(currentNumber + '0');
		String downRight = "" + Character.toString((char)(currentLetter + 1)) + (char)(currentNumber - 1 + '0');
		String down = "" + Character.toString((char)currentLetter) + (char)(currentNumber - 1 + '0');
		String downLeft = "" + Character.toString((char)(currentLetter - 1)) + (char)(currentNumber - 1 + '0');
		String left  = "" + Character.toString((char)(currentLetter - 1)) + (char)(currentNumber + '0');
		String upLeft = "" + Character.toString((char)(currentLetter - 1)) + (char)(currentNumber + 1 + '0');
		
		
		
		
		if((Chess.legalMove(hash, king, up, board) && !isCheckBool(hash, oppList, up, hash.get(king).color, board)) ||
				(Chess.legalMove(hash, king, upRight, board) && !isCheckBool(hash, oppList, upRight, hash.get(king).color, board)) ||
				(Chess.legalMove(hash, king, right, board) && !isCheckBool(hash, oppList, right, hash.get(king).color, board)) ||
				(Chess.legalMove(hash, king, downRight, board) && !isCheckBool(hash, oppList, downRight, hash.get(king).color, board)) ||
				(Chess.legalMove(hash, king, down, board) && !isCheckBool(hash, oppList, down, hash.get(king).color, board)) ||
				(Chess.legalMove(hash, king, downLeft, board) && !isCheckBool(hash, oppList, downLeft, hash.get(king).color, board))  ||
				(Chess.legalMove(hash, king, left, board) && !isCheckBool(hash, oppList, left, hash.get(king).color, board)) ||
				(Chess.legalMove(hash, king, upLeft, board) && !isCheckBool(hash, oppList, upLeft, hash.get(king).color, board))) {
			
			list.add(king);
			return false;
		}
		
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).equals(king)) {
				list.remove(i);
				i--;
			}
		}
		if(threatening instanceof pieces.Pawn) {
			answer = isCheck(hash, list, check, board);
		}
		else if(threatening instanceof pieces.Bishop) {
			answer = bishop(hash, list, check, king, board);
		}
		else if(threatening instanceof pieces.Knight) {
			answer = isCheck(hash, list, check, board);
		}
		else if(threatening instanceof pieces.Queen) {
			String first = rook(hash, list, check, king, board);
			String second = bishop(hash, list, check, king, board);
		
			if(first != null || second != null) {
				list.add(king);
				return false;
			}
		}
		else if(threatening instanceof pieces.Rook) {
			answer = rook(hash, list, check, king, board);
		}
		if(answer != null) {
			
			list.add(king);
			return false;
		}
		list.add(king);
		return true;
	}
	/**
	 * Checks for a location of a piece that can save the King from a threatening bishop
	 * @param hash the hashtable storing the type and position of each piece
	 * @param list the list of locations of pieces
	 * @param check the location of the piece that's threatening the king
	 * @param king the location of the current king 
	 * @param board the board to be printed to the console
	 * @return the location of a piece that can save the king from a bishop
	 */
	public static String bishop(Hashtable<String, piece> hash, List<String> list, String check, String king, String[][] board) {
		char currentLetter = check.charAt(0);
		int currentNumber = Character.getNumericValue(check.charAt(1));
		char nextLetter = king.charAt(0);
		int nextNumber = Character.getNumericValue(king.charAt(1));
		String a = null;
		if(nextLetter > currentLetter) {
			if(nextNumber > currentNumber) {
				currentLetter++;
				currentNumber++;
				while(nextLetter >= currentLetter && nextNumber >= currentNumber) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentLetter++;
					currentNumber++;
				}
			}
			else if(nextNumber < currentNumber) {
				currentLetter++;
				currentNumber--;
				while(nextLetter >= currentLetter && nextNumber <= currentNumber) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentLetter++;
					currentNumber--;
				}
			}
		}
		else if(currentLetter > nextLetter) {
			if(nextNumber > currentNumber) {
				currentLetter--;
				currentNumber++;
				while(nextLetter <= currentLetter && nextNumber >= currentNumber) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentLetter--;
					currentNumber++;
				}
			}
			else if(nextNumber < currentNumber) {
				currentLetter--;
				currentNumber--;
				while(nextLetter <= currentLetter && nextNumber <= currentNumber) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentLetter--;
					currentNumber--;
				}
			}
		}
		
		return a;
	}
	
	/**
	 * Checks for a location of a piece that can save the King from a threatening rook
	 * @param hash the hashtable storing the type and position of each piece
	 * @param list the list of locations of pieces
	 * @param check the location of the piece that's threatening the king
	 * @param king the location of the current king 
	 * @param board the board to be printed to the console
	 * @return the location of a piece that can save the king from a rook
	 */
	public static String rook(Hashtable<String, piece> hash, List<String> list, String check, String king, String[][] board) {
		char currentLetter = check.charAt(0);
		int currentNumber = Character.getNumericValue(check.charAt(1));
		char nextLetter = king.charAt(0);
		int nextNumber = Character.getNumericValue(king.charAt(1));
		String a = null;
		
		if(currentLetter == nextLetter) {
			if(currentNumber < nextNumber) {
				currentNumber++;
				while(currentNumber <= nextNumber) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentNumber++;
				}
			}
			if(currentNumber > nextNumber) {
				currentNumber--;
				while(currentNumber >= nextNumber) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentNumber--;
				}
			}
		}
		else if(currentNumber == nextNumber) {
			if(currentLetter < nextLetter) {
				currentLetter++;
				while(currentLetter <= nextLetter) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentLetter++;
				}
			}
			if(currentLetter > nextLetter) {
				currentLetter--;
				while(currentLetter >= nextLetter) {
					a = isCheck(hash, list, "" + Character.toString((char)currentLetter) + (char)(currentNumber + '0'), board);
					if(a != null) {
						return a;
					}
					currentLetter--;
				}
			}
		}
		
		return a;
	}
}
