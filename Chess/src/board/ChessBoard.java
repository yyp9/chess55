package board;
import java.util.*;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Queen;
import pieces.Rook;
import pieces.piece;  

/**
 * This class creates the Chess Board that will be displayed in console
 * as well as the hashtable containing the positions of pieces and their 
 * corresponding type
 * 
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.Bishop
 * @see pieces.King
 * @see pieces.Knight
 * @see pieces.Pawn
 * @see pieces.Queen
 * @see pieces.Rook
 * @see pieces.piece
 */
public class ChessBoard {
	/**
	 * This method prints the chess board to the console
	 * @param board The 2D array chess board 
	 * 		  		to be displayed on the 
	 * 				console
	 */
	public static void printCurrentBoard(String[][] board) {
		int col = 0;
		for (int row = 0; row < board.length; row++) {
			for (col = 0; col < board[row].length - 1; col++) {
				System.out.print(board[row][col] + " ");
			}
			System.out.print(board[row][col]);
			System.out.println();
		}
	}
	
	/**
	 * This method creates the chess board to be used 
	 * throughout the entire program
	 * 
	 * @return a 2D array that represents the chess board in its initial state
	 */
	public static String[][] createBoard(){
		String[][] board = {{"bR", "bN", "bB", "bQ", "bK", "bB", "bN", "bR", "8"},
							{"bp", "bp", "bp", "bp", "bp", "bp", "bp", "bp", "7"},
							{"  ", "##", "  ", "##", "  ", "##", "  ", "##", "6"},
							{"##", "  ", "##", "  ", "##", "  ", "##", "  ", "5"},
							{"  ", "##", "  ", "##", "  ", "##", "  ", "##", "4"},
							{"##", "  ", "##", "  ", "##", "  ", "##", "  ", "3"},
							{"wp", "wp", "wp", "wp", "wp", "wp", "wp", "wp", "2"},
							{"wR", "wN", "wB", "wQ", "wK", "wB", "wN", "wR", "1"},
							{" a", " b", " c", " d", " e", " f", " g", " h", ""}};
		return board;
	}
	
	/**
	 * This method creates a hashtable with key-value pairs of locations and their
	 * corresponding pieces which will be used throughout the program.
	 * 
	 * @param board a 2D array representing the chess board
	 * @return a hashtable containing key value pairs of locations in
	 * FileRank format with the values as corresponding pieces
	 * 
	 */
	public static Hashtable<String, piece> createHash(String[][] board){
		Hashtable<String, piece> hash = new Hashtable<String, piece>();

		for (int row = 0; row < board.length - 1; row++) {
			for (int col = 0; col < board[row].length - 1; col++) {
				if(board[row][col].substring(0,1).equals("b") || board[row][col].substring(0,1).equals("w")) {
					piece newPiece = new Pawn(board[row][col].substring(0,1).charAt(0));

					if(board[row][col].substring(1,2).equals("B")) {
						newPiece = new Bishop(board[row][col].substring(0,1).charAt(0));
					}
					else if(board[row][col].substring(1,2).equals("K")) {
						newPiece = new King(board[row][col].substring(0,1).charAt(0));
					}
					else if(board[row][col].substring(1,2).equals("N")) {
						newPiece = new Knight(board[row][col].substring(0,1).charAt(0));
					}
					else if(board[row][col].substring(1,2).equals("Q")) {
						newPiece = new Queen(board[row][col].substring(0,1).charAt(0));
					}
					else if(board[row][col].substring(1,2).equals("R")) {
						newPiece = new Rook(board[row][col].substring(0,1).charAt(0));
					}
					hash.put(convert(row, col), newPiece);
				}
				else {
					hash.put(convert(row, col), new piece());
				}
		
			}
		}
		return hash;
	}
	/**
	 * This method converts the row and column location of the pieces
	 * to a more usable FileRank format 
	 * 
	 * @param row the row number of the piece in int format
	 * @param col the column number of the piece in int format
	 * @return a String containg the location in FileRank format
	 * 
	 */
	public static String convert(int row, int col) {
		String c = (char)(col + 97) + "";
		String r = String.valueOf((row - 8) * -1);
		return c + r;
	}
}
