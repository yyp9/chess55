package pieces;

import java.util.Hashtable;

/**
 * This class is the default piece class
 * that represents any general chess piece
 * @author Yug Patel
 * @author Preston Peng
 *
 */
public class piece {
	/**
	 * color represents the color of the current piece,
	 * w for white
	 * b for black 
	 * o for empty by default
	 */
	public char color = 'o';
	
	/**
	 * enPassantStatus, a field to check if a pawn has moved two spaces
	 * on its first turn and is available to be captured via
	 * en Passant rule.  Set to false by default
	 */
	public boolean enPassantStatus = false;
	/**
	 * hasMoved, a field to check if this piece has moved
	 * at all from its initial location
	 */
	
	public boolean hasMoved = false;
	/**
	 * The general isLegal method for a general piece, to be used separately
	 * for each piece
	 * @param hash the hashtable storing the type and position of each piece
	 * @param start the current location of where the specified piece is
	 * @param end the location of where the specified piece ends up
	 * @param board the board to be displayed on the console
	 * @return false by default
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		return false;
	}
}
