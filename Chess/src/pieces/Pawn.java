package pieces;

import java.util.Hashtable;

/**
 * This class represents the 
 * Pawn Piece of the Chess board
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.piece
 */
public class Pawn extends piece{
	
	/**
	 * Creates a new instance of a Pawn
	 * @param c indicates the color 
	 * 			of the pawn either 
	 * 			b for black or
	 * 			w for white
	 *
	 */
	public Pawn(char c) {
		this.color = c;
	}
	
	/**
	 * Checks whether the given starting and ending positions of the pawn piece
	 * is legal.  Also checks to see if "en Passant" of the pawn is allowed if and only if
	 * two pawns are side by side to each other
	 * 
	 * @param hash the hashtable storing the type and position of each piece
	 * @param start the start position of the piece being moved in FileRank format
	 * @param end the end position of the piece being moved in FileRank format
	 * @param board the board representing the entire chess board to be displayed in console
	 * @return true if the specified move from start to end is a legal move for the Pawn piece
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		char currentLetter = start.charAt(0);
		int currentNumber = Character.getNumericValue(start.charAt(1));
		char nextLetter = end.charAt(0);
		int nextNumber = Character.getNumericValue(end.charAt(1));
		piece eatPiece = hash.get(end);
		if(eatPiece.color == this.color) {
			return false;
		}
		if(this.color == 'w') {
			if(currentLetter == nextLetter) {
				if(currentNumber + 1 == nextNumber && eatPiece.color == 'o') {
					if(enPassantStatus == true) {
						enPassantStatus = false;
					}
					return true;
				}else if(currentNumber == 2 && nextNumber == 4 && eatPiece.color == 'o') {
					enPassantStatus = true;
					return true;
				}
			}
			else if(((int)currentLetter + 1 == (int)nextLetter || (int)currentLetter - 1 == (int)nextLetter) && currentNumber + 1 == nextNumber) {
				if(eatPiece.color == 'o') {
					String currNum = String.valueOf(currentNumber);
					String oppPawnLoc = nextLetter + currNum + "";
					piece oppPiece = hash.get(oppPawnLoc);
					if(oppPiece.color == 'o' || oppPiece.color == 'w') {
						return false;
					}
					if(oppPiece instanceof Pawn) {
						if(oppPiece.enPassantStatus == true) {
							return true;
						}
					}
					return false;
				}
				else if(eatPiece.color == 'b') {
					return true;
				}
			}
		}
		else{
			if(currentLetter == nextLetter) {
				if(currentNumber - 1 == nextNumber && eatPiece.color == 'o') {
					if(enPassantStatus == true) {
						enPassantStatus = false;
					}
					return true;
				}else if(currentNumber == 7 && nextNumber == 5 && eatPiece.color == 'o'){
					enPassantStatus = true;
					return true;
				}
			}
			else if(((int)currentLetter + 1 == (int)nextLetter || (int)currentLetter - 1 == (int)nextLetter) && currentNumber - 1 == nextNumber) {
				if(eatPiece.color == 'o') {
					String currNum = String.valueOf(currentNumber);
					String oppPawnLoc = nextLetter + currNum + "";
					piece oppPiece = hash.get(oppPawnLoc);
					if(oppPiece.color == 'o' || oppPiece.color == 'b') {
						return false;
					}
					if(oppPiece instanceof Pawn) {
						if(oppPiece.enPassantStatus == true) {
							return true;
						}
					}
					return false;
				}
				else if(eatPiece.color == 'w') {
					return true;
				}
			}
		}
		return false;
	}
}
