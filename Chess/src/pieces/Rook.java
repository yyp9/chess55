package pieces;

import java.util.Hashtable;
/** 
 * This class represents the Rook piece of
 * the Chess board
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.piece
 */
public class Rook extends piece {
	/** Creates a new instance of a Rook
	 *	@param c indicates the color 
	 * 			of the rook either 
	 * 			b for black or
	 * 			w for white
	 */
	public Rook(char c) {
		this.color = c;
	}
	
	/**
	 *@param hash the hashtable storing the type and position of each piece
	 *@param start the current location of where the Rook is
	 *@param end the location of where the Rook ends up
	 *@param board the board to be displayed on the console
	 *@return true if the move from start to end is a valid move for the Rook piece
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		char currentLetter = start.charAt(0);
		int currentNumber = Character.getNumericValue(start.charAt(1));
		char nextLetter = end.charAt(0);
		int nextNumber = Character.getNumericValue(end.charAt(1));
		piece eatPiece = hash.get(end);
		if(eatPiece.color == this.color) {
			return false;
		}
		
		if(currentLetter == nextLetter) {
			if(currentNumber < nextNumber) {
				currentNumber++;
				while(currentNumber <= nextNumber) {
					if(currentNumber == nextNumber) {
						hasMoved = true;
						return true;
					}
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentNumber++;
				}
			}
			if(currentNumber > nextNumber) {
				currentNumber--;
				while(currentNumber >= nextNumber) {
					if(currentNumber == nextNumber) {
						hasMoved = true;
						return true;
					}
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentNumber--;
				}
			}
		}
		else if(currentNumber == nextNumber) {
			if(currentLetter < nextLetter) {
				currentLetter++;
				while(currentLetter <= nextLetter) {
					if(currentLetter == nextLetter) {
						hasMoved = true;
						return true;
					}
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentLetter++;
				}
			}
			if(currentLetter > nextLetter) {
				currentLetter--;
				while(currentLetter >= nextLetter) {
					if(currentLetter == nextLetter) {
						hasMoved = true;
						return true;
					}
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentLetter--;
				}
			}
		}
		return false;
	}
}
