package pieces;

import java.util.Hashtable;

/** 
 * This class represents the King piece of
 * the Chess board
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.piece
 */
public class King extends piece{
	
	/** Creates a new instance of a King
	 *	@param c indicates the color 
	 * 			of the king either 
	 * 			b for black or
	 * 			w for white
	 */
	public King(char c) {
		this.color = c;
	}
	
	/**
	 *@param hash the hashtable storing the type and position of each piece
	 *@param start the current location of where the king is
	 *@param end the location of where the king ends up
	 *@param board the board to be displayed on the console
	 *@return true if the move from start to end is a valid move for the King piece
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		char currentLetter = start.charAt(0);
		int currentNumber = Character.getNumericValue(start.charAt(1));
		char nextLetter = end.charAt(0);
		int nextNumber = Character.getNumericValue(end.charAt(1));
		piece eatPiece = hash.get(end);
		if(eatPiece.color == this.color) {
			return false;
		}
		if(currentLetter + 1 == nextLetter || currentLetter - 1 == nextLetter) {
			if(currentNumber == nextNumber || currentNumber + 1 == nextNumber || currentNumber - 1 == nextNumber) {
				hasMoved = true;
				return true;
			}
		}
		else if(currentLetter == nextLetter) {
			if(currentNumber + 1 == nextNumber || currentNumber - 1 == nextNumber) {
				hasMoved = true;
				return true;
			}
		}else if(color == 'w') {
			if(currentLetter == 'e' && currentNumber == 1) {
				if(nextLetter == 'g' && nextNumber == 1) {
					piece f1piece = hash.get("f1");
					piece g1piece = hash.get("g1");
					if(f1piece.color != 'o' || g1piece.color != 'o') {
						return false;
					}else {
						piece h1piece = hash.get("h1");
						if(h1piece instanceof Rook  && h1piece.hasMoved == false && hasMoved == false) {
							hasMoved = true;
							return true;
						}else {
							return false;
						}
					}
				} else if(nextLetter == 'c' && nextNumber == 1) {
					piece d1piece = hash.get("d1");
					piece c1piece = hash.get("c1");
					piece b1piece = hash.get("b1");
					if(d1piece.color != 'o' || c1piece.color != 'o' || b1piece.color != 'o') {
						return false;
					}else {
						piece a1piece = hash.get("a1");
						if(a1piece instanceof Rook && a1piece.hasMoved == false && hasMoved == false) {
							hasMoved = true;
							return true;
						}else {
							return false;
						}
					}
				}
			}
		}else if(color == 'b') {
			if(currentLetter == 'e' && currentNumber == 8) {
				if(nextLetter == 'g' && nextNumber == 8) {
					piece f8piece = hash.get("f8");
					piece g8piece = hash.get("g8");
					if(f8piece.color != 'o' || g8piece.color != 'o') {
						return false;
					}else {
						piece h8piece = hash.get("h8");
						if(h8piece instanceof Rook && h8piece.hasMoved == false && hasMoved == false) {
							hasMoved = true;
							return true;
						}else {
							return false;
						}
					}
				}else if(nextLetter == 'c' && nextNumber == 8) {
					piece d8piece = hash.get("d8");
					piece c8piece = hash.get("c8");
					piece b8piece = hash.get("b8");
					if(d8piece.color != 'o' || c8piece.color != 'o' || b8piece.color != 'o') {
						return false;
					}else {
						piece a8piece = hash.get("a8");
						if(a8piece instanceof Rook && a8piece.hasMoved == false && hasMoved == false) {
							hasMoved = true;
							return true;
						}else {
							return false;
						}
					}
				}
			}
		}
		return false;
	}
}
