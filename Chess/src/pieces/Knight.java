package pieces;

import java.util.Hashtable;
/** 
 * This class represents the Knight piece of
 * the Chess board
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.piece
 */
public class Knight extends piece{
	/** Creates a new instance of a Knight
	 *	@param c indicates the color 
	 * 			of the knight either 
	 * 			b for black or
	 * 			w for white
	 */
	public Knight(char c) {
		this.color = c;
	}
	/**
	 *@param hash the hashtable storing the type and position of each piece
	 *@param start the current location of where the Knight is
	 *@param end the location of where the Knight ends up
	 *@param board the board to be displayed on the console
	 *@return true if the move from start to end is a valid move for the Knight piece
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		char currentLetter = start.charAt(0);
		int currentNumber = Character.getNumericValue(start.charAt(1));
		char nextLetter = end.charAt(0);
		int nextNumber = Character.getNumericValue(end.charAt(1));
		piece eatPiece = hash.get(end);
		if(eatPiece.color == this.color) {
			return false;
		}
		if(((int)currentLetter + 1 == (int)nextLetter || (int)currentLetter - 1 == (int)nextLetter) && (Math.abs(currentNumber - nextNumber) == 2)) {
			if(this.color != hash.get(end).color) {
				return true;
			}
		}
		else if(((int)currentLetter + 2 == (int)nextLetter || (int)currentLetter - 2 == (int)nextLetter) && (Math.abs(currentNumber - nextNumber) == 1)) {
			if(this.color != hash.get(end).color) {
				return true;
			}
		}
		return false;
	}
}
