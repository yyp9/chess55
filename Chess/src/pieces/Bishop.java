package pieces;

import java.util.Hashtable;
/** 
 * This class represents the Bishop piece of
 * the Chess board
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.piece
 */
public class Bishop extends piece{	
	/** Creates a new instance of a Bishop
	 *	@param c indicates the color 
	 * 			of the bishop either 
	 * 			b for black or
	 * 			w for white
	 */
	public Bishop(char c) {
		this.color = c;
	}
	
	/**
	 *@param hash the hashtable storing the type and position of each piece
	 *@param start the current location of where the Bishop is
	 *@param end the location of where the Bishop ends up
	 *@param board the board to be displayed on the console
	 *@return true if the move from start to end is a valid move for the Bishop piece
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		char currentLetter = start.charAt(0);
		int currentNumber = Character.getNumericValue(start.charAt(1));
		char nextLetter = end.charAt(0);
		int nextNumber = Character.getNumericValue(end.charAt(1));
		piece eatPiece = hash.get(end);
		if(eatPiece.color == this.color) {
			return false;
		}
		if(nextLetter > currentLetter) {
			if(nextNumber > currentNumber) {
				currentLetter++;
				currentNumber++;
				while(nextLetter >= currentLetter && nextNumber >= currentNumber) {
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(currentLetter == nextLetter && currentNumber == nextNumber && hash.get(s).color != this.color) {
						return true;
					}

					if(hash.get(s).color != 'o') {
						return false;
					}
					currentLetter++;
					currentNumber++;
				}
			}
			else if(nextNumber < currentNumber) {
				currentLetter++;
				currentNumber--;
				while(nextLetter >= currentLetter && nextNumber <= currentNumber) {
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(currentLetter == nextLetter && currentNumber == nextNumber && hash.get(s).color != this.color) {
						return true;
					}
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentLetter++;
					currentNumber--;
				}
			}
		}
		else if(currentLetter > nextLetter) {
			if(nextNumber > currentNumber) {
				currentLetter--;
				currentNumber++;
				while(nextLetter <= currentLetter && nextNumber >= currentNumber) {
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(currentLetter == nextLetter && currentNumber == nextNumber && hash.get(s).color != this.color) {
						return true;
					}
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentLetter--;
					currentNumber++;
				}
			}
			else if(nextNumber < currentNumber) {
				currentLetter--;
				currentNumber--;
				while(nextLetter <= currentLetter && nextNumber <= currentNumber) {
					String s = "" + currentLetter + (char)(currentNumber + '0');
					if(currentLetter == nextLetter && currentNumber == nextNumber && hash.get(s).color != this.color) {
						return true;
					}
					if(hash.get(s).color != 'o') {
						return false;
					}
					currentLetter--;
					currentNumber--;
				}
			}
		}
		return false;
	}
}
