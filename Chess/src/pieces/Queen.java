package pieces;

import java.util.Hashtable;

/**
 * This class represents the
 * Queen Piece of the chess board
 * @author Yug Patel
 * @author Preston Peng
 * @see pieces.piece
 */
public class Queen extends piece {
	
	/**
	 * Creates a new instance of Queen
	 * @param c indicates the color of 
	 * 			the Queen either
	 * 			b for black or
	 * 			w for white
	 * 		  
	 */
	public Queen(char c) {
		this.color = c;
	}
	
	/**
	 * Checks whether the given starting and ending positions of the queen piece
	 * is legal.
	 * 
	 * @param hash the hashtable storing the type and position of each piece
	 * @param start the start position of the piece being moved in FileRank format
	 * @param end the end position of the piece being moved in FileRank format
	 * @param board the board representing the entire chess board to be displayed in console
	 * @return whether the specified move from start to end is a legal move for the Queen piece
	 */
	public boolean isLegal(Hashtable<String, piece> hash, String start, String end, String[][] board) {
		Bishop newBishop = new Bishop(this.color);
		Rook newRook = new Rook(this.color);
		if(newBishop.isLegal(hash, start, end, board) || newRook.isLegal(hash, start, end, board)) {
			return true;
		}
		return false;
	}
}
